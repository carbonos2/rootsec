import apt, os

def Update(cache: apt.Cache(), timestamp: str) -> None:
    # Update the database
    # Return the updated database

    will_upgrade: bool = False
    
    for pkg in cache:
      if pkg.is_upgradable:
        will_upgrade = True
        break
    
    if not will_upgrade: return
    
    backup_file = open(f'/etc/rootsec/{timestamp}.bckp', "a")
    backup_file.write(f"[Upgraded]\n")
    
    for pkg in cache:
      if pkg.is_upgradable:
        backup_file.write(f"{pkg.name} from {pkg.installed.version} to {pkg.candidate.version}\n")
        print(pkg.name, pkg.candidate.version, pkg.installed.version)