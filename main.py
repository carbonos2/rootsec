import apt, os, sys, time
from commands.update import Update

# make a dir if it doesnt exist
if not os.path.exists('/etc/rootsec/'):
    os.makedirs('/etc/rootsec/')
   
timestamp:str = time.strftime(
  "%Y%m%d-%H%M%S",
  time.localtime(time.time()))

os.open(f'/etc/rootsec/{timestamp}.bckp', os.O_CREAT)

try:
  os.open(f'~/.local')


print(f"Backup point created at /etc/rootsec/{timestamp}.bckp")
  
cache = apt.Cache()
cache.update()
cache.open(None)

args = sys.argv

if len(args) > 1:
  if args[1] == 'upgrade': Update(cache, timestamp)
  else: print('Invalid command')
else:
  print('No command given...')